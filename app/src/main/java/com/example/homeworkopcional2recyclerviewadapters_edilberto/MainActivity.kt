package com.example.homeworkopcional2recyclerviewadapters_edilberto

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.homeworkopcional2recyclerviewadapters_edilberto.producto.Producto
import com.example.homeworkopcional2recyclerviewadapters_edilberto.producto.adapter.ProductoAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val productos = mutableListOf<Producto>()
    lateinit var adaptador: ProductoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        cargarinformacion()
        configuraradaptador()
    }

    private fun configuraradaptador() {
        adaptador=ProductoAdapter(productos){
            val bundle = Bundle().apply {
                putSerializable("KEY_PRODUCTO",it)
            }
            val intent=Intent(this,MostrarPantalla::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
        recyclerviewproductos.adapter=adaptador
        recyclerviewproductos.layoutManager= LinearLayoutManager(this)

    }
    private fun cargarinformacion() {
        productos.add(Producto(1,"Abarrotes S/.10 Los Abarrotes a buen precio",R.drawable.img001))
        productos.add(Producto(2,"Verduras S/.10 las verduras verdesitas de todo",R.drawable.img002))
        productos.add(Producto(3,"Bebidas S/.10 las bebidas calientes y frias",R.drawable.img003))
        productos.add(Producto(4,"Panaderia S/.10 variedades de panes",R.drawable.img004))
        productos.add(Producto(5,"Condimentos S/.10 los condimentos para tu hogar",R.drawable.img005))
        productos.add(Producto(6,"Reposteria S/.10 variedades de tortas",R.drawable.img006))
    }
}