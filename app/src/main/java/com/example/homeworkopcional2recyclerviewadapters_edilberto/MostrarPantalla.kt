package com.example.homeworkopcional2recyclerviewadapters_edilberto

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.homeworkopcional2recyclerviewadapters_edilberto.producto.Producto
import kotlinx.android.synthetic.main.activity_mostrar_pantalla.*

class MostrarPantalla : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mostrar_pantalla)

        val bundle:Bundle?= intent.extras
        bundle?.let {
            val producto =it.getSerializable("KEY_PRODUCTO") as Producto
                tvtitulonombre.text=producto.name
            if (tvtitulonombre.text=="Abarrotes S/.10 Los Abarrotes a buen precio"){imageView1.setImageResource(R.drawable.img001)}
            if (tvtitulonombre.text=="Verduras S/.10 las verduras verdesitas de todo"){imageView1.setImageResource(R.drawable.img002)}
            if (tvtitulonombre.text=="Bebidas S/.10 las bebidas calientes y frias"){imageView1.setImageResource(R.drawable.img003)}
            if (tvtitulonombre.text=="Panaderia S/.10 variedades de panes"){imageView1.setImageResource(R.drawable.img004)}
            if (tvtitulonombre.text=="Condimentos S/.10 los condimentos para tu hogar"){imageView1.setImageResource(R.drawable.img005)}
            if (tvtitulonombre.text=="Reposteria S/.10 variedades de tortas"){imageView1.setImageResource(R.drawable.img006)}

            println("${producto.name}")
            println("${producto.foto}")
        }
    }
}